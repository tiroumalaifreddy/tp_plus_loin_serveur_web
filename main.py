from typing import Optional
import uvicorn
import requests

from fastapi import FastAPI
from pydantic import BaseModel

class Mot(BaseModel):
    id:int
    caracteres:str
    def __init__(self,id,caracteres):
        self.id=id
        self.caracteres=caracteres

app = FastAPI()

list_mots = []


@app.post("/mot")
async def create_mot(mot: Mot):
    list_mots.append(mot)

@app.get("/mots")
async def product_data():
    return list_mots

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)

